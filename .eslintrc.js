/*
 * ESLint 样式规则取自 Vue 官方推荐:
 * https://github.com/vuejs/eslint-config-vue
 * eslint.md 有一个简略的说明
 * Eslint 详细说明文档见 http://eslint.cn/docs/rules/
 * vscode 可以通过 插件 ESLint 设置 "eslint.autoFixOnSave": true 自动格式化
 */
module.exports = {
  root: true,
  parser: 'babel-eslint',
  parserOptions: {
    sourceType: 'module'
  },
  extends: [
    "eslint:recommended",
    "vue"
  ],
  globals: {
    "__debug__": true,
    "NODE_ENV": true
  },
  env: {
    "browser": true,
    "es6": true,
    "amd": true
  },
  plugins: [
    'html'
  ],
  parserOptions: {
    "ecmaVersion": 6,
    "sourceType": "module"
  },
  rules: {
    // 模式改为 最少有一个空格是为了 对象字面量属性 可以支持纵向对齐 但不强制
    "key-spacing": [2, { "beforeColon": false, "afterColon": true , "mode": "minimum"}],
    'no-debugger': 0,
    'no-console': 0,
    'no-eval': 0
  }
}
